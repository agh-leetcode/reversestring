public class Solution {
    public static void main(String[] args) {

        Solution solution = new Solution();
        solution.runTheApp();
    }

    private void runTheApp() {
        String string = "hello";
        String reversedString = reverseString2(string);
        System.out.println(reversedString);
    }

    private String reverseString(String string) {
//        return string.codePoints().boxed().sorted(Collections.reverseOrder()).collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();
        return new StringBuilder(string).reverse().toString();
    }

    //best solution
    private String reverseString2 (String string){
        char[] chars = string.toCharArray();

        int start = 0;
        int last = chars.length-1;
        while (start< last){
            char temp = chars[start];
            chars[start] = chars[last];
            chars[last] = temp;
            start++;
            last--;
        }
        return new String(chars);
    }
}
